import java.text.SimpleDateFormat;
import java.util.Calendar;

class NewThread extends Thread {
    private String currentTime;

    NewThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        //display 8 messages
        for (int i = 0; i < 8; i++) {
            //get the current time
            this.currentTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
            //display message
            System.out.println('[' + Thread.currentThread().getName() + "]-[" + currentTime + ']');
            try {
                //wait 1 second in order to display each message at an interval of 1 second
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class Exercise2 {
    public static void main(String[] args) {
        NewThread newThread1 = new NewThread("NewThread1");
        NewThread newThread2 = new NewThread("NewThread2");
        NewThread newThread3 = new NewThread("NewThread3");
        NewThread newThread4 = new NewThread("NewThread4");

        newThread1.start();
        newThread2.start();
        newThread3.start();
        newThread4.start();
    }
}